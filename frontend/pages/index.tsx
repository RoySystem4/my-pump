import type { NextPage } from "next";
import Head from "next/head";
import styles from "client/styles/pages/Home.module.css";
import { Hero } from "client/components/Hero";
import heroImage from "public/images/hero/widepump.png";
// import MypumpForm from "../client/new_components/MypumpForm";
// import Explanation from "../client/sections/Explanation";
// import AreaCTA from "../client/sections/AreaCTA";
// import TitleText from "../client/sections/TitleText";
// import CookieBar from "../client/new_components/Cookies";

const title = "MYPUMP | Express Yourself | Borstkolf Verhuur";
const description =
  "Het eenvoudig huren van een borstkolf, geen borg en vandaag beschikbaar via een onbemande unit die op verschillende locaties 24/7 toegankelijk is.";
const googleTagId = "G-Q1VTY83F3Y";

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
        <script
          async
          src={`https://www.googletagmanager.com/gtag/js?id=${googleTagId}`}
        ></script>
        <script
          dangerouslySetInnerHTML={{
            __html: `window.dataLayer = window.dataLayer || [];`,
          }}
        />
        <script
          dangerouslySetInnerHTML={{
            __html: `function gtag(){dataLayer.push(arguments)};
                    gtag('js', new Date());
                    gtag('config', '${googleTagId}');`,
          }}
        />
      </Head>

      <Hero
        src={heroImage}
        title="24/7 een borstkolf huren"
        linkText="Reserveer nu!"
        href="/#Bestellen"
        page="home"
        curved
      />
      <div className={styles.container}>
        {/* <Container>
        <Explanation />
        <MypumpForm />
        <TitleText />
        <AreaCTA />
      </Container>
      <CookieBar /> */}
      </div>
    </>
  );
};

export default Home;

// old stuff
// import React from "react";
// import { Head, Script } from "next";
// import Container from "../client/new_components/Container";
// import { Hero } from "client/components/Hero";
// import MypumpForm from "../client/new_components/MypumpForm";
// import Explanation from "../client/sections/Explanation";
// import AreaCTA from "../client/sections/AreaCTA";
// import TitleText from "../client/sections/TitleText";
// import CookieBar from "../client/new_components/Cookies";

// const title = "MYPUMP | Express Yourself | Borstkolf Verhuur";
// const description =
//   "Het eenvoudig huren van een borstkolf, geen borg en vandaag beschikbaar via een onbemande unit die op verschillende locaties 24/7 toegankelijk is.";
// const googleTagId = "G-Q1VTY83F3Y";

// export default function Home() {
//   return (
//     <>
//       <Head>
//         <title>{title}</title>
//         <meta name="description" content={description} />
//         <Script
//           async
//           src={`https://www.googletagmanager.com/gtag/js?id=${googleTagId}`}
//         ></Script>
//         <script
//           dangerouslySetInnerHTML={{
//             __html: `window.dataLayer = window.dataLayer || [];`,
//           }}
//         />
//         <script
//           dangerouslySetInnerHTML={{
//             __html: `function gtag(){dataLayer.push(arguments)};
//                     gtag('js', new Date());
//                     gtag('config', 'G-Q1VTY83F3Y-zzz');`,
//           }}
//         />
//       </Head>
//       <Hero
//         img="/images/hero/widepump.png"
//         title="24/7 een borstkolf huren"
//         linkText="Reserveer nu!"
//         href="/#Bestellen"
//         page="home"
//         curved="true"
//       />
//       <Container>
//         <Explanation />
//         <MypumpForm />
//         <TitleText />
//         <AreaCTA />
//       </Container>
//       <CookieBar />
//     </>
//   );
// }

import "client/styles/globals.scss";
import withRedux from "next-redux-wrapper";
import makeStore from "client/store";
import type { AppProps } from "next/app";
import { Navigation } from "client/components/Navigation";
import { Footer } from "client/components/Footer";

function MyApp({ Component, pageProps }: AppProps) {
  if (process.browser) {
    if ("scrollRestoration" in window.history) {
      window.history.scrollRestoration = "manual";
    }
  }

  return (
    <>
      <Navigation />
      <Component {...pageProps} />
      <Footer />
    </>
  );
}

export default withRedux(makeStore)(MyApp);

// old stuff

// import React from "react";
// import { Provider } from "react-redux";
// import App from "next/app";
// import withRedux from "next-redux-wrapper";

// import makeStore from "../client/utils/store";
// import Auth from "../client/services/auth";
// import NProgress from "nprogress";
// import Router from "next/router";

// import "../styles/globals.scss";
// import Nav from "../client/new_components/Nav";
// import Footer from "../client/new_components/Footer";
// import userActions from "../client/actions/user";

// Router.events.on("routeChangeStart", () => {
//   NProgress.start();
// });
// Router.events.on("routeChangeComplete", () => NProgress.done());
// Router.events.on("routeChangeError", () => NProgress.done());

// class MyApp extends App {
//   static async getInitialProps({ Component, ctx }) {
//     ctx.userId = Auth.getUserId(ctx);

//     const pageProps = Component.getInitialProps
//       ? await Component.getInitialProps(ctx)
//       : {};

//     Object.assign(pageProps, {
//       isAuthenticated: Auth.isAuthenticated(ctx)
//     });

//     if (pageProps.isAuthenticated) {
//       await ctx.store.dispatch(
//         userActions.verifyToken({
//           id: ctx.userId,
//           token: Auth.getJwt(ctx)
//         })
//       );
//     }

//     if (pageProps.private) {
//       Auth.redirectIfNotAuthenticated(ctx, pageProps.user);
//       Object.assign(pageProps, {
//         userId: ctx.userId
//       });
//     }

//     return { pageProps };
//   }

//   constructor(props) {
//     super(props);

//     this.state = {
//       show: true
//     };
//   }

//   render() {
//     const { Component, pageProps, store } = this.props;

//     if (process.browser) {
//       if ("scrollRestoration" in window.history) {
//         window.history.scrollRestoration = "manual";
//       }
//     }

//     return (
//       <Provider store={store}>
//         <div>
//           <Nav />
//           <Component {...pageProps} />
//           <Footer />
//         </div>
//       </Provider>
//     );
//   }
// }

// export default withRedux(makeStore)(MyApp);

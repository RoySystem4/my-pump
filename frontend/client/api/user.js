/* eslint-disable camelcase */
import fetch from "isomorphic-fetch";
import request from "../utils/request";
import { apiRoutes } from "../config/routes";

// console.log(apiRoutes.AUTH_USER_API)

class UserApi {
  static create(body) {
    return fetch(
      apiRoutes.CREATE_USER_API(),
      request({
        method: "POST",
        body,
      })
    )
      .then((res) => res.json())
      .catch((error) => error);
  }

  static login({ email, password }) {
    return fetch(
      apiRoutes.AUTH_USER_API(),
      request({
        method: "GET",
        authorization: {
          type: "basic",
          email,
          password,
        },
      })
    )
      .then((res) => res.json())
      .catch((error) => error);
  }

  static verifyToken({ id, token }) {
    return fetch(
      apiRoutes.VERIFY_TOKEN_API(id, token),
      request({
        method: "GET",
      })
    )
      .then((res) => res.json())
      .catch((error) => error);
  }

  static update(body) {
    return fetch(
      apiRoutes.UPDATE_USER_API(),
      request({
        method: "PUT",
        authorization: {
          type: "bearer",
        },
        body,
      })
    )
      .then((res) => res.json())
      .catch((error) => error);
  }

  static forgotPassword(body) {
    return fetch(
      apiRoutes.FORGOT_PASSWORD_API(),
      request({
        method: "POST",
        body,
      })
    )
      .then((res) => res.json())
      .catch((error) => error);
  }

  static updatePassword(body) {
    return fetch(
      apiRoutes.UPDATE_PASSWORD_API(),
      request({
        method: "PUT",
        body,
      })
    )
      .then((res) => res.json())
      .catch((error) => error);
  }

  static get(id, ctx = {}) {
    return fetch(
      apiRoutes.GET_USER_API(id),
      request({
        method: "GET",
        authorization: {
          type: "bearer",
        },
        ctx,
      })
    )
      .then((res) => res.json())
      .catch((error) => error);
  }

  static delete(id) {
    return fetch(
      apiRoutes.DELETE_USER_API(id),
      request({
        method: "DELETE",
        authorization: {
          type: "bearer",
        },
      })
    )
      .then((res) => res)
      .catch((error) => error);
  }
}

export default UserApi;

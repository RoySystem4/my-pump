import { combineReducers } from "redux";
import user from "./user";

const checkout = (state = {}, action) => {
  return state;
};

const cart = (state = {}, action) => {
  return state;
};

export default combineReducers({
  cart,
  checkout,
  user,
});

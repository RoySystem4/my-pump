/**
 /* Load state from local storage
 /* Used as an alternative to dependencies like redux-persist
*/
export const loadState = (initialState) => {
  if (process.browser) {
    // TODO: Fix load state and add fallback for when localStorage is not available
    const serializedState = localStorage.getItem("state");
    if (serializedState !== null) {
      const deserializedState = JSON.parse(serializedState);
      return { ...initialState, ...deserializedState };
    }
  }
  return undefined;
};

/**
 /* Save state to local storage
 /* Used as an alternative to dependencies like redux-persist
*/
export const saveState = (state, key) => {
  if (process.browser) {
    const serializedState = JSON.parse(localStorage.getItem("state"));

    if (key === "cart") {
      serializedState.cart = state;
      localStorage.setItem("state", JSON.stringify(serializedState));
    } else if (key === "checkout") {
      serializedState.checkout = state;
      localStorage.setItem("state", JSON.stringify(serializedState));
    } else if (key === "acuityCheckout") {
      serializedState.acuityCheckout = state;
      localStorage.setItem("state", JSON.stringify(serializedState));
    } else {
      localStorage.setItem(
        "state",
        JSON.stringify({ cart: state.cart, checkout: state.checkout })
      );
    }
  }
};

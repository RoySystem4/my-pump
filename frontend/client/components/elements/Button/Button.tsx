import React, { FC } from "react";
import clsx from "clsx";
import Link from "next/link";
import styles from "./Button.module.scss";

type ButtonProps = {
  label?: string;
  href?: string;
  success?: boolean;
  error?: boolean;
  disabled?: boolean;
  onClick?: () => any;
};

export const Button: FC<ButtonProps> = ({
  label,
  href = null,
  onClick = () => {},
  success = false,
  error = false,
  disabled = false,
}) => {
  return href ? (
    <Link href={href}>
      <a
        className={clsx(
          styles.button,
          success && styles.success,
          error && styles.error
        )}
      >
        {label}
      </a>
    </Link>
  ) : (
    <button
      disabled={disabled}
      onClick={onClick}
      className={clsx(
        styles.button,
        success && styles.success,
        error && styles.error
      )}
    >
      {label}
    </button>
  );
};

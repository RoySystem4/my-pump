/* eslint-disable max-len */
import React, { PureComponent } from "react";
import styles from "./Navigation.module.scss";
import { connect } from "react-redux";
import Link from "next/link";
import PropTypes from "prop-types";
// import acuityCheckoutActions from "../../actions/";
// import NavMenu from "./NavMenu";
// import Authentication from "../components/Layouts/Popup/Authentication";
// import Auth from "../services/auth";
// import userActions from "../actions/user";
// import Media from "../utils/mediaQuery";
// import login from "../shared/media/images/login.svg";
// import logout from "../shared/media/images/logout.svg?include";
/*  eslint-enable import/no-unresolved */
// import Transition from "../components/Animations/Transitions";

/**
 * Header
 */
class _Navigation extends PureComponent {
  static propTypes = {
    cart: PropTypes.object,
    user: PropTypes.object,
    checkout: PropTypes.object,
    isAuthenticated: PropTypes.bool,
    navClearBackground: PropTypes.bool,
    leftNavClear: PropTypes.bool,
    forceScroll: PropTypes.bool,
    notification: PropTypes.array,
    resetAcuity: PropTypes.func,
    arrowPreviousPage: PropTypes.bool,
    logout: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      mobile: false,
      isScrolling: false,
      visible: false,
      openPopup: false,
      timerOpen: false,
      closePopup: false,
      timerClose: false,
      openPopupLogin: false,
      timerOpenLogin: false,
      closePopupLogin: false,
      testPopup: true,
    };
    // TODO: Do we need this?
    // this.mediaQuery = new Media();
  }

  openLogin = () => {
    if (this.state.timerOpenLogin !== true && this.state.timerClose !== true) {
      this.setState({
        openPopupLogin: true,
        closePopupLogin: false,
        timerOpenLogin: true,
      });

      setTimeout(() => {
        this.setState({ timerOpenLogin: false });
      }, 500);
    }
  };

  closeLogin = () => {
    if (this.state.timerOpenLogin !== true && this.state.timerClose !== true) {
      this.setState({ closePopupLogin: true, timerClose: true });

      setTimeout(() => {
        this.setState({ openPopupLogin: false, timerClose: false });
      }, 500);
    }
  };

  logout = () => {
    // TODO: Fix Auth
    Auth.logout();
    this.props.logout();
  };

  showMenu = () => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    const { visible, isScrolling } = this.state;

    if (!visible) {
      document
        .getElementsByTagName("html")[0]
        .setAttribute("style", "overflow: hidden");
      document.body.style.overflow = "hidden";
      document.body.style.position = "relative";
      document.body.style.height = "100%";
    } else {
      document
        .getElementsByTagName("html")[0]
        .setAttribute("style", 'overflow: ""');
      document.body.style.overflow = "";
      document.body.style.position = "";
      document.body.style.height = "";
    }

    this.setState({
      visible: !visible,
      isScrolling: !isScrolling,
    });
  };

  disableTestPopup = () => {
    const { testPopup } = this.state;
    this.setState({ testPopup: false });
  };

  render() {
    const { visible, testPopup } = this.state;

    return (
      <>
        <nav className={styles.navContainer}>
          {this.state.openPopupLogin && (
            <div>Authentication here...</div>
            // TODO: review this part for authentication
            // <>
            //   <Authentication
            //     openAuth={this.state.openPopupLogin}
            //     closeAuth={this.state.closePopupLogin}
            //     closeCard={this.closeLogin}
            //   />
            // </>
          )}
          <div className={styles.logo}>
            <Link href="/">
              <img src="/images/mypumplogo.png" />
            </Link>
          </div>
          <div className={styles.nav}>
            <ul>
              <li>
                <Link href="/">MYPUMP</Link>
              </li>
              <li>
                <Link href="/faq">FAQ</Link>
              </li>
              <li>
                <Link href="/contact">Contact</Link>
              </li>
              <li>
                <Link href="/zorgaanbieders">Zorgaanbieders</Link>
              </li>
            </ul>
          </div>
          <div className={styles.right}>
            <div className={styles.user}>
              {!this.props.user.token ? (
                <img
                  src={"/images/usericons/login.svg"}
                  onClick={() => this.openLogin()}
                />
              ) : (
                <>
                  <span>
                    <img
                      src={"/images/usericons/logout.svg"}
                      onClick={() => this.logout()}
                    />
                  </span>
                </>
              )}
            </div>
            <div className={styles.hamburger}>
              <img src="/images/icons/menu.svg" onClick={this.showMenu} />
            </div>
          </div>
          {visible && (
            <div className={styles.menu}>
              <div>
                <Link href="/">
                  <a onClick={this.showMenu}>Home</a>
                </Link>
                <Link href="/faq" onClick={this.showMenu}>
                  <a onClick={this.showMenu}>FAQ</a>
                </Link>
                <Link href="/contact" onClick={this.showMenu}>
                  <a onClick={this.showMenu}>Contact</a>
                </Link>
                <Link href="/zorgaanbieders" onClick={this.showMenu}>
                  <a onClick={this.showMenu}>Zorgaanbieders</a>
                </Link>
                <div>
                  <h3>Contact</h3>
                  <p>
                    Bel ons: <a href="tel:0883636111">088 36 36 111</a>
                  </p>

                  <p>
                    Mail ons:{" "}
                    <a href="mailto: info@mypump.nl" target="_blank">
                      info@mypump.nl
                    </a>
                  </p>
                </div>

                <div>
                  <h3>Visit us</h3>
                  <p>
                    Visit our{" "}
                    <a
                      href="https://www.instagram.com/mypumpnl"
                      target="_blank"
                    >
                      Instagram
                    </a>
                  </p>
                  <p>
                    Visit our{" "}
                    <a href="https://www.facebook.com/mypumpnl" target="_blank">
                      Facebook
                    </a>
                  </p>
                </div>
              </div>
            </div>
          )}
        </nav>
        {/* {testPopup &&
          <div className={styles.testPopup}>
            <div>
              <h2>We zijn onze nieuwe site momenteel aan het testen</h2>
              <h2>Dit betekend dat reserveringen mogelijk nog niet correct werken</h2>
              <h2>Excuses voor het ongemak</h2>
              <h2>Wil je toch een borstkolf huren bel dan 0682717991</h2>
              <button onClick={this.disableTestPopup}>Ga toch door naar de site</button>
            </div>
          </div>
        } */}
      </>
    );
  }
}

const mapStateToProps = ({ user, cart, checkout, notification }) => ({
  user,
  notification,
  cart,
  checkout,
});

const mapDispatchToProps = (dispatch) => ({
  // logout: () => dispatch(userActions.logout()),
  logout: () => {},
});

export const Navigation = connect(
  mapStateToProps,
  mapDispatchToProps
)(_Navigation);

import React, { FC } from "react";
import Image, { ImageProps } from "next/image";
import { Button } from "client/components/elements/Button";
import styles from "./Hero.module.scss";

type HeroProps = {
  src: ImageProps["src"];
  title: string;
  body?: React.ReactElement;
  linkText?: string;
  href?: string;
  page: string;
  curved?: boolean;
};
export const Hero: FC<HeroProps> = ({
  src,
  title,
  body,
  linkText,
  href,
  page,
  curved,
}) => {
  return (
    <div className={styles.hero}>
      <div className={styles[`${page}`]}>
        <Image
          src={src}
          alt="Hero main website image"
          layout="fill"
          width="100%"
        />
        <div className={curved ? styles.curved : styles.content}>
          <div className={styles.title}>
            <h1>{title}</h1>
          </div>

          {body ? (
            <div className={styles.body}>
              <p>{body}</p>
            </div>
          ) : null}

          {linkText && href ? (
            <div className={styles.button} id={linkText}>
              <Button href={href} label={linkText} />
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
};

const userActions = {
  // USER
  CREATE_USER: "CREATE_USER",
  AUTH_USER: "AUTH_USER",
  VERIFY_TOKEN: "VERIFY_TOKEN",
  UPDATE_USER: "UPDATE_USER",
  GET_USER: "GET_USER",
  DELETE_USER: "DELETE_USER",
  LOGOUT: "LOGOUT",
  FORGOT_PASSWORD: "FORGOT_PASSWORD",
  UPDATE_PASSWORD: "UPDATE_PASSWORD",

  // GOOGLE MAPS
  GET_COORDINATES: "GET_COORDINATES",
};

export default userActions;
